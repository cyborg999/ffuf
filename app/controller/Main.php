<?php
namespace app\controller;

use rueckgrat\mvc\DefaultController;

/**
 * Description of Main
 *
 * @author Lenovo G470
 */
class Main extends DefaultController {
    public function __construct(){
        parent::__construct();
    }
    
    public function index(){
        echo 'test';
    }
    
    public function helloFramework(){
        echo 'Hello Framework';
    }
}
